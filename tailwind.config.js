/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      transitionProperty: {
        'height': 'height',
        'spacing': 'margin, padding',
      },
      colors: {
        'custom-orange': '#DA4B07',
        'custom-orange-hover': '#EF5106',
        'custom-black-hover': '#2F384F',
        'custom-dark': '#252c3e',
        'custom-gray': '#F9F9FB',
      },
    },
  },
  plugins: [],
}


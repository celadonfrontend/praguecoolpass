import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  changeLanguage,
  selectActiveLanguage,
} from "../../redux/features/activeLanguage";
import { useDispatch, useSelector } from "react-redux";
import { fetchMenu } from "../../redux/features/fetchMenu";
import "./style.css";
import PropTypes from "prop-types";

const NavBar = ({ translationRender }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const activeLanguage = useSelector(selectActiveLanguage);
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [isLanguageMenuOpen, setIsLanguageMenuOpen] = useState(false);
  const menu = useSelector((state) => state.fetchMenu);
  const languageSymbols = {
    en: "ENGLISH",
    cs: "ČEŠTINA",
    de: "DEUTSCH",
    es: "ESPAÑOL",
    it: "ITALIANO",
    fr: "FRANÇAIS",
    ru: "РУССКИЙ",
    pl: "POLSKI",
  };

  const [buttonLanguage, setButtonLanguage] = useState(
    languageSymbols[localStorage.getItem("activeLanguage")] || "ENGLISH"
  );

  NavBar.propTypes = {
    translationRender: PropTypes.object.isRequired,
  };

  useEffect(() => {
    dispatch(fetchMenu());
  }, [dispatch]);

  useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth >= 768) {
        setIsMenuOpen(false);
      }
    };

    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const menuRender = menu || {};

  const toggleMenu = () => {
    if (isMenuOpen) {
       document.querySelector('.navbar-open2').classList.add('navbar-close2');
       setTimeout(() => {
         setIsMenuOpen(false);
       }, 200);
    } else {
       setIsMenuOpen(true);
    }
   };

  const toggleLanguageMenu = () => {
    setIsLanguageMenuOpen(!isLanguageMenuOpen);
  };

  const handleLanguageChange = (languageCode) => {
    dispatch(changeLanguage(languageCode));
    toggleLanguageMenu();
  };

  const handleLanguageButtonClick = (language, buttonLanguage) => {
    handleLanguageChange(language);
    setButtonLanguage(buttonLanguage);
  };

  return (
    <div
      className={`fixed top-0 bg-gray-800 bg-opacity-60 h-11 flex justify-center items-center z-50 w-screen
    ${isMenuOpen ? "navbar-open" : ""}`}
    >
      <nav className="flex justify-between w-full p-5 lg:p-24">
        <div className="flex items-center">
          <button onClick={toggleMenu} className="md:hidden lg:hidden">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              className="h-6 w-6 text-orange-600"
            >
              {isMenuOpen ? (
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M6 18L18 6M6 6l12 12"
                />
              ) : (
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M4 6h16M4 12h16M4 18h16"
                />
              )}
            </svg>
          </button>
          <ul className="flex items-center text-white cursor-pointer text-xl font-bold py-1 pl-5">
            CoolPass
          </ul>
        </div>
        <ul className="justify-center items-center">
          <div className="flex flex-col sm:flex-row hidden sm:flex text-xs lg:text-base whitespace-nowrap overflow-hidden w-auto lg:ml-auto">
            <div
              onClick={() => navigate("/under-construction")}
              className="cursor-pointer text-white px-2 py-3 w-auto lg:px-2 lg:py-2 lg:m-auto"
            >
              COOLPASS/CARD
            </div>
            <div
              onClick={() => navigate("/under-construction")}
              className="text-white cursor-pointer px-2 py-3 lg:px-2 lg:py-2 lg:m-auto"
            >
              {translationRender.ATTRACTIONS}
            </div>
            <div
              onClick={() => navigate("/under-construction")}
              className="text-white cursor-pointer px-2 py-3 lg:px-2 lg:py-2 lg:m-auto"
            >
              {menuRender.data[25] &&
              menuRender.data[25].content &&
              menuRender.data[25].content[activeLanguage]
                ? menuRender.data[25].content[activeLanguage].title
                : ""}
            </div>
            <div
              onClick={() => navigate("/under-construction")}
              className="text-white cursor-pointer px-2 py-3 lg:px-2 lg:py-2 lg:m-auto "
            >
              {menuRender.data[32] &&
              menuRender.data[32].content &&
              menuRender.data[32].content[activeLanguage]
                ? menuRender.data[32].content[activeLanguage].title
                : ""}
            </div>
            <div
              onClick={() => navigate("/under-construction")}
              className="text-white cursor-pointer px-2 py-3 lg:px-2 lg:py-2 lg:m-auto "
            >
              {menuRender.data[38] &&
              menuRender.data[38].content &&
              menuRender.data[38].content[activeLanguage]
                ? menuRender.data[38].content[activeLanguage].title
                : ""}
            </div>
            <div
              onClick={() => navigate("/under-construction")}
              className="text-white cursor-pointer px-2 py-3 lg:px-2 lg:py-2 lg:m-auto "
            >
              {menuRender.data[46] &&
              menuRender.data[46].content &&
              menuRender.data[46].content[activeLanguage]
                ? menuRender.data[46].content[activeLanguage].title
                : ""}
            </div>
          </div>
        </ul>
        <ul className="pt-1 md:pt-0 justify-center items-center">
          <div className="flex lg:mr-auto">
            {!isMenuOpen && (
              <button
                className="bg-custom-orange hover:bg-custom-orange-hover text-sm text-white lg:text-base
                        sm:text-base font-bold lg:ml-3 py-1 px-3 rounded sm:py-1 sm:px-2 md:py-0 md:px-2 w-4/5
                        sm:w-auto md:w-auto lg:w-auto xl:w-auto 2xl:w-auto
                         w-auto
                        lg:relative lg:right-auto lg:top-auto justify-center items-center
                        lg:absolute lg:flex lg:w-auto whitespace-nowrap overflow-hidden"
              >
                {translationRender.BUY_NOW}
              </button>
            )}
            <button
              onClick={toggleLanguageMenu}
              className={`bg-[#252C3E] text-sm text-white hover:bg-[#2F384F] font-bold lg:ml-3 py-1 px-2 rounded-md
                sm:py-1 sm:px-2 md:py-2 md:px-5 ${
                  isMenuOpen ? "flex" : "hidden sm:flex"
                }
                items-center w-auto justify-center sm:w-auto md:w-auto lg:w-auto xl:w-auto 2xl:w-auto z-50  /* Mobil Görünüm */
                          w-auto
                    
                        /* Large Görünüm */
                        lg:relative lg:right-auto lg:top-auto justify-center items-center lg:absolute lg:flex lg:w-auto"`}
              style={{ zIndex: 50 }}
            >
              <span className="flex items-center">
                {buttonLanguage}
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-3 w-3 ml-1 inline-block"
                  fill="currentColor"
                  viewBox="0 0 24 24"
                >
                  <path d="M12 15l-6-6h12z" />
                </svg>
                {isLanguageMenuOpen && (
                  <div className="absolute top-11 bg-[#252C3E] right-2 rounded-md flex flex-col text-sm px-4 items-start z-50">
                    <div
                      onClick={() => handleLanguageButtonClick("en", "ENGLISH")}
                      className="cursor-pointer text-white font-bold m-2"
                    >
                      English
                    </div>
                    <div
                      onClick={() => handleLanguageButtonClick("cs", "ČEŠTINA")}
                      className="cursor-pointer text-white font-bold m-2"
                    >
                      Čeština
                    </div>
                    <div
                      onClick={() => handleLanguageButtonClick("de", "DEUTSCH")}
                      className="cursor-pointer text-white font-bold m-2"
                    >
                      Deutsch
                    </div>
                    <div
                      onClick={() => handleLanguageButtonClick("es", "ESPAÑOL")}
                      className="cursor-pointer text-white font-bold m-2"
                    >
                      Español
                    </div>
                    <div
                      onClick={() =>
                        handleLanguageButtonClick("it", "ITALIANO")
                      }
                      className="cursor-pointer text-white font-bold m-2"
                    >
                      Italiano
                    </div>
                    <div
                      onClick={() =>
                        handleLanguageButtonClick("fr", "FRANÇAIS")
                      }
                      className="cursor-pointer text-white font-bold m-2"
                    >
                      Français
                    </div>
                    <div
                      onClick={() => handleLanguageButtonClick("ru", "РУССКИЙ")}
                      className="cursor-pointer text-white font-bold m-2"
                    >
                      Русский
                    </div>
                    <div
                      onClick={() => handleLanguageButtonClick("pl", "POLSKI")}
                      className="cursor-pointer text-white font-bold m-2"
                    >
                      Polski
                    </div>
                  </div>
                )}
              </span>
            </button>
          </div>
        </ul>
        {isMenuOpen && (
          <div
            className={`absolute top-10 left-0 right-0 bg-[#727580] flex flex-col items-center ${
              isMenuOpen ? "navbar-open2" : "navbar-close2"
            }`}
          >
            <div
              onClick={() => navigate("/under-construction")}
              className="cursor-pointer text-white font-bold m-2 mt-5"
            >
              COOLPASS/CARD
            </div>
            <div
              onClick={() => navigate("/under-construction")}
              className="text-white font-bold m-2"
            >
              {translationRender.ATTRACTIONS}
            </div>
            <div
              onClick={() => navigate("/under-construction")}
              className="text-white font-bold cursor-pointer px-2 py-1 lg:px-1 lg:py-3 lg:m-2"
            >
              {menuRender.data[25] &&
              menuRender.data[25].content &&
              menuRender.data[25].content[activeLanguage]
                ? menuRender.data[25].content[activeLanguage].title
                : ""}
            </div>
            <div
              onClick={() => navigate("/under-construction")}
              className="text-white cursor-pointer font-bold px-2 py-1 lg:px-1 lg:py-3 lg:m-2 "
            >
              {menuRender.data[32] &&
              menuRender.data[32].content &&
              menuRender.data[32].content[activeLanguage]
                ? menuRender.data[32].content[activeLanguage].title
                : ""}
            </div>
            <div
              onClick={() => navigate("/under-construction")}
              className="text-white cursor-pointer px-2 py-1 lg:px-1 font-bold lg:py-3 lg:m-2 "
            >
              {menuRender.data[38] &&
              menuRender.data[38].content &&
              menuRender.data[38].content[activeLanguage]
                ? menuRender.data[38].content[activeLanguage].title
                : ""}
            </div>
            <div
              onClick={() => navigate("/under-construction")}
              className="text-white font-bold m-2"
            >
              {translationRender.FAQ}
            </div>
            <button
              onClick={() => navigate("/under-construction")}
              className="bg-custom-orange hover:bg-custom-orange-hover text-white m-4 font-bold mt-4 py-2 px-4 rounded"
            >
              BUY ONLINE
            </button>
          </div>
        )}
      </nav>
    </div>
  );
};

export default NavBar;

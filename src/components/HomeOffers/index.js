import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import './style.css';
import PropTypes from 'prop-types';

const HomeOffers = ({ data }) => {
    const navigate = useNavigate();
    const [hoveredIndex, setHoveredIndex] = useState(null);

    const offers = [
        {
            url:
                "https://static2.praguecoolpass.com/small_816c7b24-528c-49b7-a2b8-d78373951cac.jpg",
            title: data?.items?.[0]?.title,
            features_list: data?.items?.[0]?.features_list,
            button_text: data?.items?.[0]?.button_text,
        },
        {
            url:
                "https://static2.praguecoolpass.com/small_bd3dde92-e556-4038-a57a-6ae816f9f8e0.jpg",
            title: data?.items?.[1]?.title,
            features_list: data?.items?.[1]?.features_list,
            button_text: data?.items?.[1]?.button_text,
        },
        {
            url:
                "https://static2.praguecoolpass.com/small_39d99d84-0e14-4928-ba41-adbe2f0db233.jpg",
            title: data?.items?.[2]?.title,
            features_list: data?.items?.[2]?.features_list,
            button_text: data?.items?.[2]?.button_text,

        },
        {
            url:
                "https://static2.praguecoolpass.com/small_5742190a-de3d-40c0-9902-78e9daa80af8.jpg",
            title: data?.items?.[3]?.title,
            features_list: data?.items?.[3]?.features_list,
            button_text: data?.items?.[3]?.button_text,
        },
    ];

    const handleMouseEnter = (index) => {
        setHoveredIndex(index);
    };

    const handleMouseLeave = () => {
        setHoveredIndex(null);
    };

    return (
        <div className="flex flex-wrap justify-center items-center rounded-lg  overflow-x-hidden">
            {offers.map((offer, index) => (
                <div
                    key={index}
                    className="w-full sm:w-full md:w-full lg:w-[48%] relative flex items-center justify-center offer-container"
                    onMouseEnter={() => handleMouseEnter(index)}
                    onMouseLeave={handleMouseLeave}
                >
                    <div className="relative">
                        <img src={offer.url} className="w-full rounded-lg mx-auto" alt="Offer" />
                        {hoveredIndex === index && (
                            <div className="absolute inset-0 bg-black bg-opacity-70 flex flex-col items-center justify-center rounded-lg p-4">
                                <p className="text-white font-bold text-[0.7rem] md:text-xl mb-1 lg:mb-4">{offer.title}</p>
                                <div className="text-white text-[0.5rem] sm:text-sm md:text-base m-1 lg:m-3" dangerouslySetInnerHTML={{ __html: offer.features_list }} /> 
                                <button className="bg-orange-500 text-white px-4 py-1 mt-4 mb-2 rounded-lg text-xs md:text-base"
                                    onClick={() => navigate('/under-construction')}>{offer.button_text}</button>
                            </div>
                        )}
                    </div>
                    {hoveredIndex !== index && (
                        <div className="absolute sm:w-4/5 h-1/5 mt-auto bottom-5 m-5 w-[%60] flex items-center justify-center font-bold bg-custom-orange bg-opacity-60 text-white text-center py-4 rounded-lg">
                            {offer.title}
                        </div>
                    )}
                </div>
            ))}
        </div>
    );
};

HomeOffers.propTypes = {
    data: PropTypes.shape({
        items: PropTypes.arrayOf(
            PropTypes.shape({
                title: PropTypes.string,
                features_list: PropTypes.string,
                button_text: PropTypes.string,
            })
        ),
    }),
};

export default HomeOffers;
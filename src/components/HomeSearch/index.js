import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import activeLanguage from "../../redux/features/activeLanguage";
import PropTypes from "prop-types";

const HomeSearch = ({ translationRender }) => {
  const [searchTerm, setSearchTerm] = useState("");
  const [attractions, setAttractions] = useState([]);
  const [filteredAttractions, setFilteredAttractions] = useState([]);
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const navigate = useNavigate();
  const searchInputRef = useRef(null);

  HomeSearch.propTypes = {
    translationRender: PropTypes.object.isRequired,
  };

  useEffect(() => {
    axios
      .get("https://api2.praguecoolpass.com/object/attraction")
      .then((response) => {
        setAttractions(response.data);
        console.log(response.data);
        setFilteredAttractions(response.data);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }, []);

  const translation = translationRender || {};

  useEffect(() => {
    const results = attractions.filter(
      (attraction) =>
        searchTerm &&
        attraction.content.en.title
          .toLowerCase()
          .includes(searchTerm.toLowerCase())
    );
    setFilteredAttractions(results);
  }, [searchTerm, attractions, activeLanguage]);

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
    setIsMenuOpen(true);
  };

  const handleResultClick = (title) => {
    setSearchTerm(title);
    setIsMenuOpen(false);
  };

  const handleButtonClick = () => {
    if (searchTerm) {
      const matchFound = filteredAttractions.some(
        (attraction) =>
          attraction.content.en.title.toLowerCase() === searchTerm.toLowerCase()
      );
      if (matchFound) {
        navigate("/under-construction");
      } else {
        console.log("No matching attraction found.");
      }
    } else {
      searchInputRef.current.focus();
    }
  };

  return (
    <div className="relative flex flex-col lg:flex-row items-center justify-center z-10 w-full">
      <div className="relative flex flex-row w-full justify-center items-center">
        <div className="flex items-center bg-white justify-center rounded-lg shadow-md w-[95%]">
          <input
            ref={searchInputRef}
            className="rounded-l-lg w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none h-10"
            id="search"
            type="text"
            placeholder={translation.SEARCH}
            value={searchTerm}
            onChange={handleSearchChange}
          />
          <div className="p-2">
            <svg
              className="w-4 h-4 text-gray-500"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
              ></path>
            </svg>
          </div>
          {isMenuOpen && searchTerm && (
            <div className="absolute left-0 top-7 w-full mx-auto bg-white rounded-lg shadow-md overflow-y-auto max-h-[200px]">
              {filteredAttractions.length > 0 ? (
                filteredAttractions.map((attraction) => (
                  <div
                    key={attraction._id}
                    className="p-2 cursor-pointer"
                    onClick={() =>
                      handleResultClick(attraction.content.en.title)
                    }
                  >
                    {attraction.content.en.title}
                  </div>
                ))
              ) : (
                <div className="p-2">{translation.SEARCH_not_found}</div>
              )}
            </div>
          )}
        </div>
      </div>
      <div
        className="bg-custom-orange text-white text-center flex justify-center cursor-pointer lg:ml-5 items-center rounded-lg w-[95%] lg:w-1/2 h-10"
        onClick={handleButtonClick}
      >
        {translation.APP_LETS_GO}
      </div>
    </div>
  );
};

export default HomeSearch;

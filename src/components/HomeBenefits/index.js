import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

const HomeBenefits = ({ data }) => {
    const [selectedItem, setSelectedItem] = useState(data?.[0]);

    useEffect(() => {
        if (data && data.length > 0) {
            setSelectedItem(data[0]);
        }
    }, [data]);

    const handleTitleClick = (item) => {
        setSelectedItem(item);
    };

    return (
        <div className="flex flex-col md:flex-row justify-between items-start">
            <div className="w-full md:w-2/3">
                {data?.map((item, index) => (
                    <div key={index} onClick={() => handleTitleClick(item)}>
                        <div
                        className="cursor-pointer bg-custom-dark hover:bg-[#2F384F] m-1
                        font-semibold text-white text-lg leading-7 uppercase p-2 sm:p-3 md:p-4 lg:p-5 xl:p-6">
                            {item.title}
                        </div>
                        {selectedItem === item && (
                            <div
                            className="bg-custom-gray ml-5 font-sans text-base text-gray-800 leading-6"
                            dangerouslySetInnerHTML={{ __html: item.text }} />
                        )}
                    </div>
                ))}
            </div>
            <div className="w-1/2 md:w-1/2 relative" style={{ marginTop: '20px' }}>
                <div className="relative flex">
                    <img
                    src="https://praguecoolpass.com/img/mobile.87cdac93.png"
                    className='w-1/2 z-10' style={{ position: 'relative', right: '-10%', bottom: '-10%' }} />
                    <img
                    src="https://praguecoolpass.com/img/prague-card-image.670e8103.png"
                    className="w-1/2 absolute top-1/2 left-1/2 transform -translate-x-1/5 -translate-y-1/4 z-0 rounded-lg" />
                </div>
            </div>
        </div>
    );
};

HomeBenefits.propTypes = {
    data: PropTypes.arrayOf(
        PropTypes.shape({
            title: PropTypes.string.isRequired,
            text: PropTypes.string.isRequired,
        }),
    ),
};

export default HomeBenefits;

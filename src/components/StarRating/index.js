import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

const StarRating = ({ rating }) => {
    const stars = [];
    for (let i = 1; i <= 5; i++) {
        stars.push(
            <span key={i} className={`star ${i <= rating ? 'filled' : ''}`}>
                ★
            </span>
        );
    }

    return <div className="star-rating">{stars}</div>;
};

StarRating.propTypes = {
    rating: PropTypes.number.isRequired,
};

export default StarRating;
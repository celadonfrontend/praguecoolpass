import React from 'react';
import PropTypes from 'prop-types';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import './style.css'
import HomeSearch from '../HomeSearch';
import { useSelector } from 'react-redux';

const HomeSlider = ({ data, translationRender }) => {

    const activeLanguage = useSelector((state) => state.activeLanguage.value);

    return (
        <div>
            <Carousel showArrows={false} autoPlay={true} stopOnHover={true} swipeable={true} infiniteLoop={true} showStatus={false}
                showThumbs={false}>
                <img src="https://static2.praguecoolpass.com/6940c4df-4580-4889-9aeb-88143df63419.jpg"

                />
                <img src="https://static2.praguecoolpass.com/46cb54f4-18b2-4289-82e1-66b5a4c1bfa6.jpg"

                />
                <img src="https://static2.praguecoolpass.com/9162c41d-5154-4abd-9e42-aecc92336019.jpg"

                />
                <img src="https://static2.praguecoolpass.com/b2cb0d42-9ece-4e8d-a539-da32771632cb.jpg"

                />
            </Carousel>
            <div className="absolute bottom-48 top-6 sm:top-auto lg:bottom-32 lg:ml-20 left-0 lg:left-8 flex flex-col items-start justify-center p-4 sm:mt-10">
                <div className="font-custom font-bold text-3xl leading-130 uppercase text-white mb-5 sm:text-5xl">
                    {data.content?.[activeLanguage]?.title}
                </div>
                <div className="font-custom font-bold text-md leading-130 text-white sm:text-base md:text-base lg:text-3xl">
                    {data.content?.[activeLanguage]?.subtitle}
                </div>
                <div className='mt-2 sm:mt-10 flex hidden lg:flex'>
                    <div className='flex-grow sm:flex-grow-0'>
                        <HomeSearch translationRender={translationRender}/>
                    </div>
                </div>
            </div>
            <div className="absolute bottom-4 right-4 mb-12 lg:mb-10 text-white text-[0.7rem] sm:text-2xs">
                <div>{translationRender.HEADER_photo_by_label} {data.mainImage?.author}</div>
            </div>
            <div className="bg-custom-orange w-full h-auto sm:h-12 flex justify-center items-center text-white font-sans font-bold 
                text-sm leading-snug tracking-wider self-center font-normal sm:text-xs md:text-md">
                <div className='m-1 font-bold'>{data.content?.[activeLanguage]?.header_banner}</div>
            </div>
            <div className='mt-2 sm:mt-10 flex flex-col w-full items-center justify-center lg:hidden'>
                <div className="flex flex-col items-center justify-center w-full">
                    <HomeSearch className="w-full" translationRender={translationRender}/>
                </div>
            </div>
            </div>
    )
};

HomeSlider.propTypes = {
    data: PropTypes.shape({
        content: PropTypes.objectOf(PropTypes.shape({
            title: PropTypes.string,
            subtitle: PropTypes.string,
            header_banner: PropTypes.string,
        })),
        mainImage: PropTypes.shape({
            author: PropTypes.string,
        }),
    }).isRequired,
    translationRender: PropTypes.object.isRequired,
};

export default HomeSlider;
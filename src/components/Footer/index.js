import React, { useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';

const Footer = ({ translationRender }) => {
    const navigate = useNavigate();
    const emailInputRef = useRef(null);
    const subscribeButtonRef = useRef(null);

    const validateEmail = (email) => {
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return regex.test(email);
    };

    const handleSubscribeClick = () => {
        const email = emailInputRef.current.value;
        if (!email || !validateEmail(email)) {
            emailInputRef.current.focus();
        } else {
            navigate('/under-construction');
        }
    };

    return (
        <div className='bg-[#252C3E] overflow-auto flex flex-col justify-between'>
            <div className='text-white flex flex-col lg:flex-row justify-between m-3 lg:m-7 lg:ml-10 lg:mr-10'>
                <ul className='flex flex-col lg:m-8 text-sm font-bold flex-grow lg:w-1/4 w-1/2'>
                    <a onClick={() => navigate('/under-construction')} className='m-1 cursor-pointer'>CoolPass / Prague Card</a>
                    <a onClick={() => navigate('/under-construction')} className='m-1 cursor-pointer'>{translationRender.FOOTER_USING_COOLPASS}</a>
                    <a onClick={() => navigate('/under-construction')} className='m-1 cursor-pointer'>{translationRender.how_you_save}</a>
                    <a onClick={() => navigate('/under-construction')} className='m-1 cursor-pointer'>{translationRender.FOOTER_get_your_pass}</a>
                    <a onClick={() => navigate('/under-construction')} className='m-1 cursor-pointer'>{translationRender.FOOTER_sales_points}</a>
                    <a onClick={() => navigate('/under-construction')} className='m-1 cursor-pointer'>{translationRender.FOOTER_reviews}</a>
                </ul>
                <ul className='flex flex-col mt-5 lg:m-8 text-sm font-bold flex-grow lg:w-1/4 w-1/2'>
                    <a onClick={() => navigate('/under-construction')} className='m-1 cursor-pointer'>{translationRender.ATTRACTIONS}</a>
                    <a onClick={() => navigate('/under-construction')} className='m-1 cursor-pointer'>{translationRender.FOOTER_sightseeing_tours}</a>
                    <a onClick={() => navigate('/under-construction')} className='m-1 cursor-pointer'>{translationRender.FOOTER_areas}</a>
                    <a onClick={() => navigate('/under-construction')} className='m-1 cursor-pointer'>{translationRender.FOOTER_closures}</a>
                    <a onClick={() => navigate('/under-construction')} className='m-1 cursor-pointer'>{translationRender.FOOTER_whats_on}</a>
                    <a onClick={() => navigate('/under-construction')} className='m-1 cursor-pointer'>{translationRender.FOOTER_contact_us}</a>
                </ul>
                <ul className='flex flex-col mt-5 lg:m-8 text-sm font-bold flex-grow lg:w-1/4 w-1/2'>
                    <div
                        onClick={() => navigate('/under-construction')}
                        className='bg-custom-orange cursor-pointer h-12 p-2 w-3/4 flex items-center justify-center rounded-lg'>
                        {translationRender.FAQ}
                    </div>
                    <a onClick={() => navigate('/under-construction')} className='m-1 cursor-pointer'>{translationRender.FOOTER_about_us}</a>
                    <a
                        onClick={() => navigate('/under-construction')}
                        className='m-1 cursor-pointer'>{translationRender.FOOTER_terms_and_conditions}</a>
                    <a
                        onClick={() => navigate('/under-construction')}
                        className='m-1 cursor-pointer'>{translationRender.FOOTER_cancellation_and_refund}</a>
                </ul>
                <ul className='flex flex-col mt-5 lg:m-8 flex-grow lg:w-1/4 w-1/2'>
                    <a className="uppercase">{translationRender.DOWNLOAD}</a>
                    <a>{translationRender.FOOTER_prague_coolpass_app}</a>
                    <img
                        onClick={() => navigate('/under-construction')}
                        src='https://praguecoolpass.com/img/appstore.9b997285.png'
                        className='w-6/7 mt-2 mb-2 cursor-pointer' />
                    <img
                        onClick={() => navigate('/under-construction')}
                        src='https://praguecoolpass.com/img/googleplay.01cc5f25.png'
                        className='w-6/7 mt-2 mb-2 cursor-pointer' />
                </ul>
                <div className='flex flex-col mt-5 lg:ml-8 flex-grow'>
                    <div>{translationRender.NEWS_AND_UPDATES}</div>
                    <div className='flex flex-row items-center'>
                        <input
                            ref={emailInputRef}
                            type='text'
                            placeholder='Your email address'
                            className='p-4 focus:outline-none text-black mt-4 mb-4 w-64 rounded-l-lg' />
                        <div
                            ref={subscribeButtonRef}
                            className='bg-custom-orange h-14 w-auto flex items-center cursor-pointer justify-center rounded-r-lg'
                            onClick={handleSubscribeClick}>
                            <a className="uppercase p-2 ">{translationRender.EMAIL_SUBSCRIBE}</a></div>
                    </div>
                    <div className='text-2xl font-bold'>CoolPass 2020-2024</div>
                    <div className='text-2xl font-bold'>Prague Card 1992-2024</div>
                    <div className='mt-10 text-white text-xs'>
                        {translationRender.ALL_RIGHTS_RESERVED_LABEL}
                    </div>
                </div>
            </div>
        </div>
    );
};

Footer.propTypes = {
    translationRender: PropTypes.object.isRequired,
};

export default Footer;

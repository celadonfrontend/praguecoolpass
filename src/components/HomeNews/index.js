import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchNews } from '../../redux/features/fetchNews';
import { useNavigate } from 'react-router-dom';
import { fetchTranslations } from '../../redux/features/fetchTranslation';
import PropTypes from 'prop-types';

const HomeNews = ({ translationRender }) => {
    const dispatch = useDispatch();
    const news = useSelector((state) => state.fetchNews);
    const navigate = useNavigate();

    HomeNews.propTypes = {
        translationRender: PropTypes.object.isRequired,
    };

    useEffect(() => {
        dispatch(fetchNews());
        dispatch(fetchTranslations());
    }, [dispatch]);

    const newsData = [
        {
            url: 'https://static2.praguecoolpass.com/small_5d4afa1e-9d9b-4655-b940-2127a18829c7.jpg',
        },
        {
            url: 'https://static2.praguecoolpass.com/small_2e8722bf-a831-4cc6-8a44-42083fab9f84.jpg',
        },
    ];
    const [isExpanded] = useState(false);

    const content = news.data?.content?.en || {};
    const truncatedText = content.text?.length > 590 ? content.text.substring(0, 590) + '...' : content.text;

    return (
        <div className="flex flex-col lg:flex-col md:flex-col sm:flex-row justify-between items-center">
            <div className="w-full md:w-full flex flex-col md:flex-row mb-5 md:mb-5">
                <img src={newsData[0].url} className="w-full sm:w-full md:w-1/2 rounded-lg" alt="News" />
                <div className="flex flex-col justify-between items-start h-full md:h-auto md:ml-5">
                    <div className="font-bold text-lg uppercase text-gray-800 mb-2" dangerouslySetInnerHTML={{ __html: content.title }}></div>
                    <div
                    className=" mt-2 text-sm text-gray-800 select-none"
                    dangerouslySetInnerHTML={{ __html: isExpanded ? content.text : truncatedText }}></div>
                    {content.text?.length > 500 && (
                        <button
                        className="text-custom-orange hover:text-custom-orange text-xs underline uppercase mt-5"
                        onClick={() => navigate('/under-construction')}>
                            {translationRender.READ_MORE}
                        </button>
                    )}
                </div>
            </div>
            <div className="w-full md:w-full flex flex-col md:flex-row mt-5 md:m-5">
                <div className="flex flex-col justify-between items-start h-full md:h-auto md:ml-5 order-2 md:order-1">
                    <div className="font-bold text-lg uppercase text-gray-800 lg:mr-5 mb-3 lg:mb-0"
                    dangerouslySetInnerHTML={{ __html: content.title }}></div>
                    <div
                    className="lg:mr-5 text-sm text-gray-800 select-none"
                    dangerouslySetInnerHTML={{ __html: isExpanded ? content.text : truncatedText }}></div>
                    {content.text?.length > 500 && (
                        <button
                        className="text-custom-orange hover:text-custom-orange text-xs underline mt-5 uppercase"
                        onClick={() => navigate('/under-construction')}>
                        {translationRender.READ_MORE}
                    </button>
                    )}
                </div>
                <img src={newsData[1].url} className="w-full md:w-1/2 rounded-lg order-1 md:order-2" alt="News2" />
            </div>
            <div
            className="bg-custom-orange text-white px-8 py-3 rounded-md m-4
            lg:self-end  w-full cursor-pointer sm:w-auto flex items-center justify-center"
                onClick={() => navigate('/under-construction')}>
                {translationRender.SEE_ALL_NEWS}
            </div>
        </div>
    );
};

export default HomeNews;

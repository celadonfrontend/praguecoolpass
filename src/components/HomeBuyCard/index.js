import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchBuyCard } from '../../redux/features/fetchBuyCard';
import { fetchTranslations } from '../../redux/features/fetchTranslation';
import Slider from 'react-slick';
import './style.css';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';

const HomeBuyCard = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const cards = useSelector((state) => state.fetchBuyCard);
    const translation = useSelector((state) => state.fetchTranslations);
    const activeLanguage = useSelector((state) => state.activeLanguage.value);

    useEffect(() => {
        dispatch(fetchBuyCard());
        dispatch(fetchTranslations());
    }, [dispatch]);

    const cardsToRender = cards.data?.cards ? cards.data.cards : [];
    const translationRender = translation.data[activeLanguage] || {};

    HomeBuyCard.propTypes = {
        data: PropTypes.any,
    };

    function SampleNextArrow(props) {
        const { className, style, onClick, currentSlide } = props;
        const isLastSlide = currentSlide === 5;
        const arrowStyle = {
            ...style,
            opacity: isLastSlide ? 0.5 : 1,
        };
        return (
            <img src="https://praguecoolpass.com/img/right-arrow-orange.44f57960.svg" className={className} style={arrowStyle} onClick={onClick} />
        );
    }

    SampleNextArrow.propTypes = {
        className: PropTypes.string,
        style: PropTypes.object,
        onClick: PropTypes.func,
        currentSlide: PropTypes.number,
    };

    SamplePrevArrow.propTypes = {
        className: PropTypes.string,
        style: PropTypes.object,
        onClick: PropTypes.func,
        currentSlide: PropTypes.number,
    };

    function SamplePrevArrow(props) {
        const { className, style, onClick, currentSlide } = props;
        const isFirstSlide = currentSlide === 0;

        const arrowStyle = {
            ...style,
            opacity: isFirstSlide ? 0.5 : 1,
            transform: 'rotate(180deg) translateY(10px)',
        };
        return (
            <img src="https://praguecoolpass.com/img/right-arrow-orange.44f57960.svg" className={className} style={arrowStyle} onClick={onClick} />
        );
    }

    const settings = {
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        outerEdgeLimit: false,
        responsive: [
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 0,
                },
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    initialSlide: 1,
                    dots: true,
                },
            },
        ],
    };

    const [adultCounts, setAdultCounts] = useState([]);
    const [studentCounts, setStudentCounts] = useState([]);

    useEffect(() => {
        if (cardsToRender.length > 0) {
            setAdultCounts(cardsToRender.map(() => 0));
            setStudentCounts(cardsToRender.map(() => 0));
        }
    }, [cardsToRender]);

    const incrementAdultCount = (index) => {
        setAdultCounts((prevCounts) => {
            const newCounts = [...prevCounts];
            newCounts[index] += 1;
            return newCounts;
        });
    };

    const decrementAdultCount = (index) => {
        setAdultCounts((prevCounts) => {
            const newCounts = [...prevCounts];
            if (newCounts[index] > 0) {
                newCounts[index] -= 1;
            }
            return newCounts;
        });
    };

    const incrementStudentCount = (index) => {
        setStudentCounts((prevCounts) => {
            const newCounts = [...prevCounts];
            newCounts[index] += 1;
            return newCounts;
        });
    };

    const decrementStudentCount = (index) => {
        setStudentCounts((prevCounts) => {
            const newCounts = [...prevCounts];
            if (newCounts[index] > 0) {
                newCounts[index] -= 1;
            }
            return newCounts;
        });
    };

    const calculateTotalPrice = (card, index) => {
        const adultPrice = card.products[0].price;
        const studentPrice = card.products[1].price;
        const adultCount = adultCounts[index] || 0;
        const studentCount = studentCounts[index] || 0;
        return adultCount * adultPrice + studentCount * studentPrice;
    };

    return (
        <div className='relative'>
            <Slider {...settings}>
                {cardsToRender.map((card, index) => (
                    <div key={index} >
                        <div className="flex flex-col items-center text-white justify-center bg-[#252C3E] rounded-t-lg h-[100px]">
                            <div className="text-xl font-bold">{card.name.default}</div>
                            <div className="text-sm">{translationRender.BUY_PRAGUE_CARD_COOL_PASS}</div>
                        </div>
                        <div className='bg-[#F9F9FB]'>
                            <div className="text-lg flex justify-center mr-20">{translationRender.PRICE}</div>
                            <div className='flex flex-row justify-between items-center m-3'>
                                <div className=' w-1/3'>{translationRender.ADULT_PRICE}</div>
                                <div className='w-1/3 font-bold flex justify-center'>{card.products[0].price} EUR</div>
                                <div className='flex flex-row items-center'>
                                    <div className='flex flex-row justify-between'>
                                        <div
                                            onClick={() => decrementAdultCount(index)}
                                            className=' bg-white p-2 rounded-lg flex justify-center items-center h-[48px] w-[48px]'>
                                            <span style={{
                                                height: '0',
                                                maxHeight: '0',
                                                width: '14px',
                                                border: '1px solid #252c3e',
                                                backgroundColor: '#252c3e',
                                                borderRadius: '1px',
                                            }} className="absolute"></span>
                                        </div>
                                        <div
                                            className='bg-white p-3 rounded-lg font-bold flex justify-center items-center text-xl h-[48px] w-[64px]'>
                                            {adultCounts[index]}
                                        </div>
                                        <div
                                            onClick={() => incrementAdultCount(index)}
                                            className='bg-white p-3 rounded-lg flex justify-center items-center h-[48px] w-[48px]'>
                                            <span style={{
                                                position: 'absolute',
                                                width: '0',
                                                height: '14px',
                                                border: '1px solid #252c3e',
                                                backgroundColor: '#252c3e',
                                                borderRadius: '1px',
                                            }} className="absolute"></span>
                                            <span style={{
                                                height: '0',
                                                maxHeight: '0',
                                                width: '14px',
                                                border: '1px solid #252c3e',
                                                backgroundColor: '#252c3e',
                                                borderRadius: '1px',
                                            }} className="absolute"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='flex flex-row justify-between items-center m-3'>
                                <div className='w-1/3'>
                                    <div >{translationRender.STUDENT_PRICE}</div>
                                    <div>{translationRender.CHILD_PRICE}</div>
                                </div>
                                <div className='w-1/3 font-bold flex justify-center'>{card.products[1].price} EUR</div>
                                <div className='flex flex-row items-center'>
                                    <div className='flex flex-row justify-between'>
                                        <div
                                            onClick={() => decrementStudentCount(index)}
                                            className='bg-white p-2 rounded-lg flex justify-center items-center h-[48px] w-[48px]'>
                                            <span style={{
                                                height: '0',
                                                maxHeight: '0',
                                                width: '14px',
                                                border: '1px solid #252c3e',
                                                backgroundColor: '#252c3e',
                                                borderRadius: '1px',
                                            }} className="absolute"></span>
                                        </div>
                                        <div
                                            className='bg-white p-3 rounded-lg font-bold flex justify-center items-center text-xl h-[48px] w-[64px]'>
                                            {studentCounts[index]}
                                        </div>
                                        <div
                                            onClick={() => incrementStudentCount(index)}
                                            className='bg-white p-3 rounded-lg flex justify-center items-center h-[48px] w-[48px]'>
                                            <span style={{
                                                position: 'absolute',
                                                width: '0',
                                                height: '14px',
                                                border: '1px solid #252c3e',
                                                backgroundColor: '#252c3e',
                                                borderRadius: '1px',
                                            }} className="absolute"></span>
                                            <span style={{
                                                height: '0',
                                                maxHeight: '0',
                                                width: '14px',
                                                border: '1px solid #252c3e',
                                                backgroundColor: '#252c3e',
                                                borderRadius: '1px',
                                            }} className="absolute"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div
                                className='flex justify-center m-5'>
                                {translationRender.YOUR_PRICE}
                                <div className='font-bold ml-1'>{calculateTotalPrice(card, index)} EUR</div></div>
                            <div
                                onClick={() => navigate('/under-construction')}
                                className='bg-custom-orange cursor-pointer text-white flex text-md
                                    justify-center font-bold items-center h-[60px] rounded-b-lg'>
                                {translationRender.CALCULATOR_COMPLETE_BOOKING_BTN}
                            </div>
                        </div>
                    </div>
                ))}
            </Slider>
            <div className='grid grid-cols-1 lg:grid-cols-3 gap-4 lg:gap-8 mt-12'>
                <div className='col-span-1 lg:col-span-1 w-full'>
                    <ul>
                        <li className="custom-list-item text-sm text-gray-800 leading-4 mb-2 pl-2">{translationRender.CALCULATOR_card_validity}</li>
                        <li
                            className="custom-list-item text-sm text-gray-800 leading-4 mb-2 pl-2">
                            {translationRender.CALCULATOR_child_card_validity_tip}
                        </li>
                    </ul>
                </div>
                <div className='col-span-1 lg:col-span-1 w-full'>
                    <ul className='w-full'>
                        <li className="custom-list-item text-sm text-gray-800 leading-4 mb-2 pl-2">{translationRender.CALCULATOR_student_id_info}</li>
                    </ul>
                </div>
                <div className='col-span-1 ml-2 lg:col-span-1 w-full text-sm font-bold flex flex-col justify-between'>
                    <div>{translationRender.ADULT_AGE}</div>
                    <div>{translationRender.STUDENT_AGE}</div>
                    <div>{translationRender.CHILD_AGE}</div>
                </div>
            </div>
        </div>
    );
};

export default HomeBuyCard;

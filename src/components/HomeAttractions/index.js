import React from 'react';
import { useEffect, useState } from 'react';
import { fetchTopAttractions } from '../../redux/features/fetchTopAttractions';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './style.css';
import PropTypes from 'prop-types';

const HomeAttractions = ({ translationRender }) => {
    const dispatch = useDispatch();
    const topAttractions = useSelector((state) => state.fetchTopAttractions);
    const navigate = useNavigate();
    const [isSliding, setIsSliding] = useState(false);
    const [hoveredIndex, setHoveredIndex] = useState(null);
    const [activeHearts, setActiveHearts] = useState([]);
    const activeLanguage = useSelector((state) => state.activeLanguage.value);

    useEffect(() => {
        dispatch(fetchTopAttractions());
    }, [dispatch]);

    const isDataAvailable = topAttractions.data && topAttractions.data.length > 0;
    const attractions = isDataAvailable ? [
        {
            url: 'https://static2.praguecoolpass.com/small_c757f3d2-c948-430c-89d1-ea4cd45b4634.jpg',
            text: `${topAttractions.data[0].content?.[activeLanguage]?.title}`,
            subtitle: `${topAttractions.data[0].content?.[activeLanguage]?.subtitle}`,
        },
        {
            url: 'https://static2.praguecoolpass.com/small_7311e70d-d382-400a-92a4-fae87bb917a8.jpg',
            text: `${topAttractions.data[1].content?.[activeLanguage]?.title}`,
            subtitle: `${topAttractions.data[1].content?.[activeLanguage]?.subtitle}`,
        },
        {
            url: 'https://static2.praguecoolpass.com/small_e355f6ab-122a-44d0-b907-8e5abe0fc447.jpg',
            text: `${topAttractions.data[2].content?.[activeLanguage]?.title}`,
            subtitle: `${topAttractions.data[2].content?.[activeLanguage]?.subtitle}`,
        },
        {
            url: 'https://static2.praguecoolpass.com/small_397ef1fc-8a9e-4337-8d77-49f13fbddbda.jpg',
            text: `${topAttractions.data[3].content?.[activeLanguage]?.title}`,
            subtitle: `${topAttractions.data[3].content?.[activeLanguage]?.subtitle}`,
        },

        {
            url: 'https://static2.praguecoolpass.com/small_bd3dde92-e556-4038-a57a-6ae816f9f8e0.jpg',
            text: `${topAttractions.data[4].content?.[activeLanguage]?.title}`,
            subtitle: `${topAttractions.data[4].content?.[activeLanguage]?.subtitle}`,
        },
        {
            url: 'https://static2.praguecoolpass.com/small_6b334ea9-eb8e-46d1-b434-a253802b1204.jpg',
            text: `${topAttractions.data[5].content?.[activeLanguage]?.title}`,
            subtitle: `${topAttractions.data[5].content?.[activeLanguage]?.subtitle}`,
        },

        {
            url: 'https://static2.praguecoolpass.com/small_97cca5a8-ddf8-4bce-b9e6-807232d68f15.jpg',
            text: `${topAttractions.data[5].content?.[activeLanguage]?.title}`,
            subtitle: `${topAttractions.data[5].content?.[activeLanguage]?.subtitle}`,
        },

        {
            url: 'https://static2.praguecoolpass.com/small_35308084-e45b-410e-bc35-7abb3e34471b.jpg',
            text: `${topAttractions.data[6].content?.[activeLanguage]?.title}`,
            subtitle: `${topAttractions.data[6].content?.[activeLanguage]?.subtitle}`,
        },

        {
            url: 'https://static2.praguecoolpass.com/small_d013e18e-681c-4b0a-a813-e436a5d91f14.jpg',
            text: `${topAttractions.data[7].content?.[activeLanguage]?.title}`,
            subtitle: `${topAttractions.data[7].content?.[activeLanguage]?.subtitle}`,
        },

        {
            url: 'https://static2.praguecoolpass.com/small_999665f4-58b9-421f-b30f-f23c22944650.jpg',
            text: `${topAttractions.data[8].content?.[activeLanguage]?.title}`,
            subtitle: `${topAttractions.data[8].content?.[activeLanguage]?.subtitle}`,
        },
    ] : [];


    const handleClick = () => {
        if (!isSliding) {
            navigate('/under-construction');
        }
    };

    const handleSlideChange = () => {
        setIsSliding(true);
        setTimeout(() => setIsSliding(false), 500);
    };

    function SampleNextArrow(props) {
        const { className, style, onClick, currentSlide } = props;
        const isLastSlide = currentSlide === 6;
        const arrowStyle = {
            ...style,
            opacity: isLastSlide ? 0.5 : 1,
        };
        return (
            <img src="https://praguecoolpass.com/img/right-arrow.7fb8afe3.svg" className={className} style={arrowStyle} onClick={onClick} />
        );
    }

    SampleNextArrow.propTypes = {
        className: PropTypes.string,
        style: PropTypes.object,
        onClick: PropTypes.func,
        currentSlide: PropTypes.number,
    };

    function SamplePrevArrow(props) {
        const { className, style, onClick, currentSlide } = props;
        const isFirstSlide = currentSlide === 0;
        const arrowStyle = {
            ...style,
            opacity: isFirstSlide ? 0.5 : 1,
        };
        return (
            <img src="https://praguecoolpass.com/img/left-arrow.4841114c.svg" className={className} style={arrowStyle} onClick={onClick} />
        );
    }

    SamplePrevArrow.propTypes = {
        className: PropTypes.string,
        style: PropTypes.object,
        onClick: PropTypes.func,
        currentSlide: PropTypes.number,
    };

    const settings = {
        infinite: false,
        beforeChange: handleSlideChange,
        afterChange: handleSlideChange,
        slidesToShow: 4,
        slidesToScroll: 4,
        initialSlide: 0,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        responsive: [
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 0,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    initialSlide: 0,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    initialSlide: 0,
                },
            },
        ],
    };

    const handleMouseEnter = (index) => {
        setHoveredIndex(index);
    };

    const handleMouseLeave = () => {
        setHoveredIndex(null);
    };

    const toggleHeart = (index, event) => {
        event.stopPropagation();
        setActiveHearts((prevState) => ({
            ...prevState,
            [index]: !prevState[index],
        }));
    };

    const heartIconStyle = {
        position: 'absolute',
        bottom: '10px',
        right: '10px',
        width: '30px',
        zIndex: '1',
    };

    return (
        <div className="relative" >
            <Slider {...settings}>
                {attractions.map((attraction, index) => (
                    <div key={index} className="w-full justify-center items-center rounded-xl relative attraction-container" onClick={handleClick} >
                        <img src={attraction.url} alt="attraction" className="w-full object-cover rounded-xl h-[200px]" />
                        <div
                        className={`absolute bottom-0 left-0 right-0 bg-black bg-opacity-50 text-white p-2 
                        transform transition-transform duration-500 h-20 hover:h-32 rounded-bl-xl rounded-br-xl flex items-center`}
                        onMouseEnter={() => handleMouseEnter(index)} onMouseLeave={handleMouseLeave}>
                            <div className="flex justify-between items-center">
                                <div>
                                    <h2 className="text-sm font-bold mb-1 w-[99%] lg:w-full">{attraction.text}</h2>
                                    {hoveredIndex === index && (
                                        <div className='text-xs w-[90%]' dangerouslySetInnerHTML={{ __html: attraction.subtitle }} />
                                    )}
                                </div>
                                <div style={heartIconStyle}
                                className="text-sm p-1 rounded-tr rounded-br cursor-pointer z-1"
                                onClick={(event) => toggleHeart(index, event)}>
                                    <img src={activeHearts[index] ?
                                    'https://praguecoolpass.com/img/heart-active.72445abc.svg':
                                    'https://praguecoolpass.com/img/heart-disable.e975f7bf.svg'} alt="heart"
                                        className='width-full' />
                                </div>
                            </div>
                        </div>
                        <div className="absolute top-2 right-0 bg-yellow-500 text-sm p-1 rounded-tl rounded-bl">
                            {translationRender.ATTRACTIONS_label_included} {translationRender.ATTRACTIONS_label_with_pass}
                        </div>
                    </div>
                ))}
            </Slider>
        </div>
    );
};

HomeAttractions.propTypes = {
    translationRender: PropTypes.object.isRequired,
};

export default HomeAttractions;

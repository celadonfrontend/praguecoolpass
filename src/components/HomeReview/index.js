import React, { useEffect, useState } from "react";
import Slider from "react-slick";
import { useSelector, useDispatch } from "react-redux";
import { fetchHomeReview } from "../../redux/features/fetchHomeReview";
import StarRating from "../StarRating";
import { useNavigate } from "react-router-dom";
import PropTypes from "prop-types";

const HomeReview = ({ translationRender }) => {
  const dispatch = useDispatch();
  const homeReview = useSelector((state) => state.fetchHomeReview);
  const [randomReviews, setRandomReviews] = useState([]);
  const [expandedReviews, setExpandedReviews] = useState({});
  const [cardHeights, setCardHeights] = useState({});
  const navigate = useNavigate();

  HomeReview.propTypes = {
    translationRender: PropTypes.shape({
      REVIEWS_see_all: PropTypes.string,
      REVIEWS_write_your_opinion: PropTypes.string,
    }),
  };

  useEffect(() => {
    dispatch(fetchHomeReview());
  }, [dispatch]);

  useEffect(() => {
    if (homeReview.data) {
      const allReviews = homeReview.data;
      const randomSubset = getRandomSubset(allReviews, 15);
      setRandomReviews(randomSubset);
    }
  }, [homeReview.data]);

  const getRandomSubset = (array, subsetSize) => {
    const shuffledArray = [...array].sort(() => Math.random() - 0.5);
    return shuffledArray.slice(0, subsetSize);
  };

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    return date.toLocaleDateString("en-US", {
      year: "numeric",
      month: "long",
      day: "numeric",
    });
  };

  const toggleExpanded = (index) => {
    setExpandedReviews((prevState) => ({
      ...prevState,
      [index]: !prevState[index],
    }));
    setCardHeights((prevHeights) => ({
      ...prevHeights,
      [index]: prevHeights[index] === "auto" ? "300px" : "auto",
    }));
  };

  function SampleNextArrow(props) {
    const { className, style, onClick, currentSlide } = props;
    const isLastSlide = currentSlide === 12;
    const arrowStyle = {
      ...style,
      opacity: isLastSlide ? 0.5 : 1,
    };
    return (
      <img
        src="https://praguecoolpass.com/img/right-arrow.7fb8afe3.svg"
        className={className}
        style={arrowStyle}
        onClick={onClick}
        alt="right-arrow"
      />
    );
  }

  SampleNextArrow.propTypes = {
    className: PropTypes.string,
    style: PropTypes.object,
    onClick: PropTypes.func,
    currentSlide: PropTypes.number,
  };

  function SamplePrevArrow(props) {
    const { className, style, onClick, currentSlide } = props;
    const isFirstSlide = currentSlide === 0;
    const arrowStyle = {
      ...style,
      opacity: isFirstSlide ? 0.5 : 1,
    };
    return (
      <img
        src="https://praguecoolpass.com/img/left-arrow.4841114c.svg"
        className={className}
        style={arrowStyle}
        onClick={onClick}
      />
    );
  }

  SamplePrevArrow.propTypes = {
    className: PropTypes.string,
    style: PropTypes.object,
    onClick: PropTypes.func,
    currentSlide: PropTypes.number,
  };

  const settings = {
    infinite: false,
    slidesToShow: 3,
    initialSlide: 0,
    slidesToScroll: 3,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 0,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true,
          swipe: true,
        },
      },
    ],
  };

  return (
    <div>
      <Slider {...settings}>
        {randomReviews.map((item, index) => (
          <div key={index} className="w-1/2">
            <div
              className="flex flex-col justify-start bg-[#F9F9FB] p-4 mb-10 lg:mb-0"
              style={{
                height: cardHeights[index] || "300px",
                overflow: "auto",
              }}
            >
              <div>
                <StarRating rating={item.rating} />
                <div className="text-xl font-bold mb-2">{item.title}</div>
                <div className="text-sm font-semibold text-gray-800 opacity-90 mt-2.5">
                  {formatDate(item.date)}
                </div>
              </div>
              <div className="overflow-hidden flex-grow">
                <div
                  className={`${expandedReviews[index] ? "block" : "hidden"}`}
                  style={{ fontSize: "0.8rem" }}
                >
                  {item.text}
                  {expandedReviews[index] && (
                    <button
                      onClick={() => toggleExpanded(index)}
                      className="text-black-500 font-bold text-sm px-1"
                    >
                    less
                    </button>
                  )}
                </div>
                <div
                  className={`${expandedReviews[index] ? "hidden" : "block"}`}
                  style={{ fontSize: "0.8rem" }}
                >
                  {item.text.substring(0, 190)}...
                  {item.text.length > 190 && (
                    <button
                      onClick={() => toggleExpanded(index)}
                      className="text-black-500 font-bold text-sm p-1"
                    >
                      more
                    </button>
                  )}
                </div>
              </div>
              <div>
                <div className="text-sm text-gray-500">
                  {item.name}, {item.place}
                </div>
              </div>
            </div>
          </div>
        ))}
      </Slider>
      <div className="flex flex-col lg:flex-row justify-center items-center lg:justify-end mt-5">
        <div
          onClick={() => navigate("/under-construction")}
          className="px-4 py-2 uppercase border border-black m-3 w-full lg:w-auto rounded-lg text-black-500 rounded cursor-pointer text-center font-semibold"
        >
          {translationRender.REVIEWS_see_all}
        </div>
        <div
          onClick={() => navigate("/under-construction")}
          className="px-4 py-2 uppercase border border-black m-3 w-full lg:w-auto rounded-lg text-black-500 rounded cursor-pointer text-center font-semibold"
        >
          {translationRender.REVIEWS_write_your_opinion}
        </div>
      </div>
    </div>
  );
};

export default HomeReview;

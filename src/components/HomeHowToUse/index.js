import React from 'react';
import './style.css';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import PropTypes from 'prop-types';

const HomeHowToUse = ({ data }) => {
    const howToUse = [
        {
            url: 'https://static2.praguecoolpass.com/2d8de848-336e-4d62-9520-e53b45a1f170.jpg',
            description: data?.[0],
            step: 1,
        },
        {
            url: 'https://static2.praguecoolpass.com/c33a3103-55bf-4fe8-82d2-62b15a0c9d61.jpg',
            description: data?.[1],
            step: 2,
        },
        {
            url: 'https://static2.praguecoolpass.com/f1d60286-2511-42d2-bf86-455a0efa2306.jpg',
            description: data?.[2],
            step: 3,
        },
        {
            url: 'https://static2.praguecoolpass.com/84696934-6835-48da-9a85-0e5475b9f199.jpg',
            description: data?.[3],
            step: 4,
        },
    ];

    HomeHowToUse.propTypes = {
        data: PropTypes.array,
    };

    const settings = {
        infinite: false,
        slidesToShow: 4,
        swipe: false,
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 0,
                    swipe: true,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 0,
                    swipe: true,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                    swipe: true,
                },
            },
        ],
    };

    return (
        <Slider {...settings}>
            {howToUse.map((use, index) => (
                <div key={index} className="flex flex-col justify-center items-center relative w-full md:w-1/4 lg:w-1/5 xl:w-1/4 m-2">
                    <img src={use.url} className="w-full rounded-lg" alt="How to use" />
                    <div className="text-center p-4">
                        <h2 className="text-2xl font-bold">{use.step}</h2>
                        <p className="mt-2 text-sm text-center text-gray-800">{use.description}</p>
                    </div>
                </div>
            ))}
        </Slider>
    );
};

export default HomeHowToUse;

import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';
import { createStore } from 'redux';
import rootReducer from '../../redux/reducers';
import HomeReview from './HomeReview';

jest.mock('../../redux/features/fetchHomeReview', () => ({
 fetchHomeReview: jest.fn(),
}));

const renderWithRouter = (ui, { route = '/' } = {}) => {
 const history = createMemoryHistory({ initialEntries: [route] });
 return {
    ...render(<Router history={history}>{ui}</Router>),
    history,
 };
};

const renderWithRedux = (ui, { initialState } = {}) => {
 const store = createStore(rootReducer, initialState);
 return {
    ...render(<Provider store={store}>{ui}</Provider>),
    store,
 };
};

describe('HomeReview', () => {
 it('renders without crashing', () => {
    renderWithRedux(<HomeReview />);
 });

 it('fetches reviews on mount', () => {
    renderWithRedux(<HomeReview />);
    expect(fetchHomeReview).toHaveBeenCalled();
 });

 it('displays reviews', async () => {
    const { getByText } = renderWithRedux(<HomeReview />, {
      initialState: { fetchHomeReview: { data: [{ title: 'Test Review', text: 'This is a test review.' }] } },
    });

    await waitFor(() => {
      expect(getByText('Test Review')).toBeInTheDocument();
    });
 });

 it('toggles review expansion', () => {
    const { getByText } = renderWithRedux(<HomeReview />, {
      initialState: { fetchHomeReview: { data: [{ title: 'Test Review', text: 'This is a test review.' }] } },
    });

    fireEvent.click(getByText('more'));
    expect(getByText('less')).toBeInTheDocument();
 });

 it('navigates to under-construction page', () => {
    const { getByText } = renderWithRouter(<HomeReview />);
    fireEvent.click(getByText('See All'));
    expect(window.location.pathname).toBe('/under-construction');
 });
});
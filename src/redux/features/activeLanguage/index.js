import { createSlice } from "@reduxjs/toolkit";

export const activeLanguageSlice = createSlice({
  name: "activeLanguage",
  initialState: {
    value: localStorage.getItem("activeLanguage") || "en",
  },
  reducers: {
    changeLanguage: (state, action) => {
      state.value = action.payload;
      localStorage.setItem("activeLanguage", action.payload);
    },
  },
});

export const { changeLanguage } = activeLanguageSlice.actions;

export const selectActiveLanguage = (state) => state.activeLanguage.value;

export default activeLanguageSlice.reducer;

import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    data: [],
    loading: false,
    error: false,
};

export const fetchHomeReview = createAsyncThunk("fetchHomeReview", async () => {
    const response = await axios.get("https://api2.praguecoolpass.com/review/card");

    return response.data;
});

const fetchHomeReviewSlice = createSlice({
    name: "homeReview",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchHomeReview.pending, (state) => {
                state.loading = true;
                state.error = false;
            })
            .addCase(fetchHomeReview.fulfilled, (state, action) => {
                state.data = action.payload;
                state.loading = false;
                state.error = false;
            })
            .addCase(fetchHomeReview.rejected, (state) => {
                state.loading = false;
                state.error = true;
            });
    },
});

export default fetchHomeReviewSlice.reducer;

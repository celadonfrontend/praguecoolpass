import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';

const initialState = {
    data: [],
    loading: false,
    error: false,
};

export const fetchNews = createAsyncThunk('fetchNews', async () => {
    const response = await axios.get('https://api2.praguecoolpass.com/news');

    return response.data[0];
});

const fetchNewsSlice = createSlice({
    name: 'news',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchNews.pending, (state) => {
                state.loading = true;
                state.error = false;
            })
            .addCase(fetchNews.fulfilled, (state, action) => {
                state.data = action.payload;
                state.loading = false;
                state.error = false;
            })
            .addCase(fetchNews.rejected, (state) => {
                state.loading = false;
                state.error = true;
            });
    },
});

export default fetchNewsSlice.reducer;

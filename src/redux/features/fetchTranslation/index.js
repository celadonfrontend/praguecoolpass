import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    data: [],
    loading: false,
    error: false,
};

export const fetchTranslations = createAsyncThunk("fetchTranslations", async () => {
    const response = await axios.get("https://api2.praguecoolpass.com/translation");

    return response.data;
});

const fetchTranslationsSlice = createSlice({
    name: "translations",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchTranslations.pending, (state) => {
                state.loading = true;
                state.error = false;
            })
            .addCase(fetchTranslations.fulfilled, (state, action) => {
                state.data = action.payload;
                state.loading = false;
                state.error = false;
            })
            .addCase(fetchTranslations.rejected, (state) => {
                state.loading = false;
                state.error = true;
            });
    },
});

export default fetchTranslationsSlice.reducer;

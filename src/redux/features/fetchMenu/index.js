import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    data: [],
    loading: false,
    error: false,
};

export const fetchMenu = createAsyncThunk("fetchMenu", async () => {
    const response = await axios.get("https://api2.praguecoolpass.com/menu");

    return response.data;
});

const fetchMenuSlice = createSlice({
    name: "menu",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchMenu.pending, (state) => {
                state.loading = true;
                state.error = false;
            })
            .addCase(fetchMenu.fulfilled, (state, action) => {
                state.data = action.payload;
                state.loading = false;
                state.error = false;
            })
            .addCase(fetchMenu.rejected, (state) => {
                state.loading = false;
                state.error = true;
            });
    },
});

export default fetchMenuSlice.reducer;

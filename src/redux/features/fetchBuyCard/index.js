import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    data: [],
    loading: false,
    error: false,
};

export const fetchBuyCard = createAsyncThunk("fetchBuyCard", async () => {
    const response = await axios.get("https://api2.praguecoolpass.com/cardCategories?eshopId=77a85a2a-6b84-4d79-b856-dfafc14340a0");

    return response.data;
});

const fetchBuyCardSlice = createSlice({
    name: "buyCard",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchBuyCard.pending, (state) => {
                state.loading = true;
                state.error = false;
            })
            .addCase(fetchBuyCard.fulfilled, (state, action) => {
                state.data = action.payload;
                state.loading = false;
                state.error = false;
            })
            .addCase(fetchBuyCard.rejected, (state) => {
                state.loading = false;
                state.error = true;
            });
    },
});

export default fetchBuyCardSlice.reducer;

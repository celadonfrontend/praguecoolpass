import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    data: [],
    loading: false,
    error: false,
};

export const fetchHome = createAsyncThunk("fetchHome", async () => {
    const response = await axios.get("https://api2.praguecoolpass.com/pages/");

    return response.data[0];
});

const fetchHomeSlice = createSlice({
    name: "home",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchHome.pending, (state) => {
                state.loading = true;
                state.error = false;
            })
            .addCase(fetchHome.fulfilled, (state, action) => {
                state.data = action.payload;
                state.loading = false;
                state.error = false;
            })
            .addCase(fetchHome.rejected, (state) => {
                state.loading = false;
                state.error = true;
            });
    },
});

export default fetchHomeSlice.reducer;

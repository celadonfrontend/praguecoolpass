import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    data: [],
    loading: false,
    error: false,
};

export const fetchTopAttractions = createAsyncThunk("fetchTopAttractions", async () => {
    const response = await axios.get("https://api2.praguecoolpass.com/object/attraction/top-attractions");

    return response.data;
});

const fetchTopAttractionsSlice = createSlice({
    name: "topAttractions",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchTopAttractions.pending, (state) => {
                state.loading = true;
                state.error = false;
            })
            .addCase(fetchTopAttractions.fulfilled, (state, action) => {
                state.data = action.payload;
                state.loading = false;
                state.error = false;
            })
            .addCase(fetchTopAttractions.rejected, (state) => {
                state.loading = false;
                state.error = true;
            });
    },
});

export default fetchTopAttractionsSlice.reducer;

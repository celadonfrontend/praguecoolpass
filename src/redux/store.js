import { configureStore } from "@reduxjs/toolkit";
import fetchHomeReducer from "./features/fetchHome";
import fetchTopAttractionsReducer from "./features/fetchTopAttractions";
import fetchNewsReducer from "./features/fetchNews";
import fetchBuyCardReducer from "./features/fetchBuyCard";
import fetchHomeReviewReducer from "./features/fetchHomeReview";
import activeLanguageReducer from "./features/activeLanguage";
import fetchTranslationsReducer from "./features/fetchTranslation";
import fetchMenuReducer from "./features/fetchMenu";

export default configureStore({
    reducer: {
        fetchHome: fetchHomeReducer,
        fetchTopAttractions: fetchTopAttractionsReducer,
        fetchNews: fetchNewsReducer,
        fetchBuyCard: fetchBuyCardReducer,
        fetchHomeReview: fetchHomeReviewReducer,
        activeLanguage: activeLanguageReducer,
        fetchTranslations: fetchTranslationsReducer,
        fetchMenu: fetchMenuReducer,
    },
});
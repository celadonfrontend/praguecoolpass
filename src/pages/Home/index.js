import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchHome } from '../../redux/features/fetchHome';
import NavBar from '../../components/NavBar';
import HomeSlider from '../../components/HomeSlider';
import HomeAttractions from '../../components/HomeAttractions';
import HomeBenefits from '../../components/HomeBenefits';
import HomeOffers from '../../components/HomeOffers';
import HomeHowToUse from '../../components/HomeHowToUse';
import HomeNews from '../../components/HomeNews';
import HomeBuyCard from '../../components/HomeBuyCard';
import Footer from '../../components/Footer';
import HomeReview from '../../components/HomeReview';
import StarRating from '../../components/StarRating';
import { selectActiveLanguage } from '../../redux/features/activeLanguage';

function Home() {
  const dispatch = useDispatch();
  const home = useSelector((state) => state.fetchHome);
  const activeLanguage = useSelector(selectActiveLanguage);
  const translation = useSelector((state) => state.fetchTranslations);
  const translationRender = translation.data[activeLanguage] || {};

  useEffect(() => {
    dispatch(fetchHome());
  }, [dispatch]);

  return (
    <div className="overflow-x-hidden">
      <div className="relative">
        <NavBar translationRender={translationRender}/>
        <HomeSlider data={home.data} translationRender={translationRender}/>
      </div>
      <div
        className="mx-4 sm:mx-6 md:mx-8 lg:mx-10 xl:mx-40 my-4 sm:my-6 md:my-8 lg:my-10 xl:my-10 font-bold text-2xl leading-7 text-uppercase text-gray-900"
      >
        {translationRender.HOME_top_attractions_title}
      </div>
      <div className="mx-4 sm:mx-6 md:mx-8 lg:mx-9 xl:mx-40 my-4 sm:my-6 md:my-8 lg:my-10 xl:my-10">
        <HomeAttractions data={home.data?.content?.[activeLanguage]} translationRender={translationRender}/>
      </div>
      <div
        className="mx-4 sm:mx-6 md:mx-8 lg:mx-10 xl:mx-40 my-4 sm:my-6 md:my-8 lg:my-10 xl:my-10 font-bold text-2xl leading-7 text-uppercase text-gray-900"
      >
        {translationRender.HOME_benefits_title}
      </div>
      <div className="mx-4 sm:mx-6 md:mx-8 lg:mx-9 xl:mx-40 my-4 sm:my-6 md:my-8 lg:my-10 xl:my-10">
        <HomeBenefits data={home.data?.content?.[activeLanguage]?.benefits?.items} text={home.data?.content?.[activeLanguage]?.offers?.offers_title} />

      </div>
      <div
        className="mx-4 sm:mx-6 md:mx-8 lg:mx-10 xl:mx-40 my-4 sm:my-6 md:my-8 lg:my-10 xl:my-10 font-bold text-2xl leading-7 text-uppercase text-gray-900"
      >
        {translationRender.HOME_offers_title}
      </div>
      <div className="sm:mx-6 md:mx-8 lg:mx-9 xl:mx-40 my-4 sm:my-6 md:my-8 lg:my-10 xl:my-10">
        <HomeOffers data={home.data?.content?.[activeLanguage]?.offers} />
      </div>
      <div
        className="mx-4 sm:mx-6 md:mx-8 lg:mx-10 xl:mx-40 my-4 sm:my-6 md:my-8 lg:my-10 xl:my-10 font-bold text-2xl leading-7 text-uppercase text-gray-900"
      >
        {translationRender.HOME_how_to_use_title}
      </div>
      <div className="mx-4 sm:mx-6 md:mx-8 lg:mx-9 xl:mx-40 my-4 sm:my-6 md:my-8 lg:my-10 xl:my-10">
        <HomeHowToUse data={home.data?.content?.[activeLanguage]?.how_to_use?.descriptions} />
      </div>
      <div
        className="mx-4 mt-10 sm:mx-6 md:mx-8 lg:mx-10 xl:mx-40 my-4 sm:my-6 md:my-8 lg:my-10 xl:my-10 font-bold text-2xl leading-7 text-uppercase text-gray-900"
      >
        {translationRender.HOME_news_title}
      </div>
      <div className="mx-4 sm:mx-6 md:mx-8 lg:mx-9 xl:mx-40 my-4 sm:my-6 md:my-8 lg:my-10 xl:my-10">
        <HomeNews data={home.data?.content?.[activeLanguage]?.news} translationRender={translationRender}/>
      </div>
      <div
        className="mx-4 sm:mx-6 md:mx-8 lg:mx-10 xl:mx-40 my-4 sm:my-6 md:my-8 lg:my-10 xl:my-10 font-bold text-2xl leading-7 text-uppercase text-gray-900"
      >
        {translationRender.BUY_COOLPASS_PRAGUE_CARD}
      </div>
      <div className="mx-4 sm:mx-6 md:mx-8 lg:mx-9 xl:mx-40 my-4 sm:my-6 md:my-8 lg:my-10 xl:my-10">
        <HomeBuyCard/>
      </div>
      <div
        className="mx-4 sm:mx-6 md:mx-8 lg:mx-10 xl:mx-40 my-4 flex flex-col lg:flex-row justify-between sm:my-6 md:my-8 lg:my-10 xl:my-10 font-bold text-2xl leading-7 text-uppercase text-gray-900"
      >
        {translationRender.REVIEWS_what_do_customers_say}
        <div className="flex flex-row">
          <div className="mr-5">4.5</div>
          <StarRating rating={4} />
        </div>
      </div>
      <div className="mx-4 sm:mx-6 md:mx-8 lg:mx-9 xl:mx-40 my-4 sm:my-6 md:my-8 lg:my-10 xl:my-10">
        <HomeReview translationRender={translationRender}/>
      </div>
      <div>
        <Footer translationRender={translationRender}/>
      </div>
    </div>
  );
}

export default Home;

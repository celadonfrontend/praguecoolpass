import React from 'react';
import './style.css';

const UnderConstruction = () => {
    return (
        <div className="under-construction">
            <h1 className="title">Under Construction</h1>
            <p className="message">We&apos;re working hard to bring you the best experience possible.</p>
            <p className="message">Thank you for your patience.</p>
        </div>
    );
};

export default UnderConstruction;
import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import { Provider } from 'react-redux';
import Home from './pages/Home';
import store from './redux/store';
import UnderConstruction from './pages/UnderConstruction';

/**
 * The main App component that sets up the routing and Redux store.
 * It renders the Home and UnderConstruction pages based on the URL path.
 * @return {ReactElement} The main App component.
 */
function App() {
  return (
    <Provider store={store}>
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/under-construction" element={<UnderConstruction />} />
        </Routes>
      </Router>
    </Provider>
  );
}

export default App;
